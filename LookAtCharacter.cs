﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCharacter : MonoBehaviour
{

    public Transform character;
    [SerializeField] private float followSpeedSmooth = 10;
    // Use this for initialization


    public void AssignTarget(Transform target)
    {
        character = target;

    }
    // Update is called once per frame
    void Update()
    {
        if (character != null)
        {
            var lookAtCharacterSmooth = Quaternion.LookRotation(character.position - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, lookAtCharacterSmooth, followSpeedSmooth * Time.deltaTime);
        }
    }
}
