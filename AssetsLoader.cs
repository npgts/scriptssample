﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetsLoader : MonoBehaviour
{

    private void OnEnable()
    {
        StartCoroutine(LoadPlayer());
        StartCoroutine(LoadLandscape());
        StartCoroutine(LoadPowerUps());
    }

    IEnumerator LoadPlayer()
    {
        AssetBundleCreateRequest bundleReq = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "player.1"));
        yield return bundleReq;

        AssetBundle myLoadedAssetBundle = bundleReq.assetBundle;
        var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>("box");
        yield return assetLoadRequest;

        GameObject prefab = assetLoadRequest.asset as GameObject;
        Instantiate(prefab);

        myLoadedAssetBundle.Unload(false);
    }

    IEnumerator LoadLandscape()
    {
        AssetBundleCreateRequest bundleReq = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "environment.1"));
        yield return bundleReq;

        AssetBundle myLoadedAssetBundle = bundleReq.assetBundle;
        var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>("Environment");
        yield return assetLoadRequest;

        GameObject prefab = assetLoadRequest.asset as GameObject;
        Instantiate(prefab);

        myLoadedAssetBundle.Unload(false);
    }

    IEnumerator LoadPowerUps()
    {
        AssetBundleCreateRequest bundleReq = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, "powerups.1"));
        yield return bundleReq;

        AssetBundle myLoadedAssetBundle = bundleReq.assetBundle;
        var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>("SpawnPoints");
        yield return assetLoadRequest;

        GameObject prefab = assetLoadRequest.asset as GameObject;
        Instantiate(prefab);

        myLoadedAssetBundle.Unload(false);
    }
}
