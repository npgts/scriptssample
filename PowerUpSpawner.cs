﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PowerUpSpawner : MonoBehaviour
{

    [SerializeField] public PowerUp[] powerUpSOList;
    private float inflationTime = 2;
    public GameManager.PowerUpEnum powerUpType;

    private void Start()
    {
        Invoke("SpawnPowerUp", 1);
    }

    public void OnPickUp()
    {

        Invoke("SpawnPowerUp", 4);
    }

    private void SpawnPowerUp()
    {
        int pUpType = Random.Range(0, powerUpSOList.Length);
        switch (pUpType)
        {
            case 0:
                StartCoroutine(Inflate(Instantiate(powerUpSOList[0].powerUpGameOBject,
           transform.position, Quaternion.identity, this.transform).transform));
                powerUpType = GameManager.PowerUpEnum.Time;
                break;
            case 1:
                StartCoroutine(Inflate(Instantiate(powerUpSOList[1].powerUpGameOBject,
           transform.position, Quaternion.identity, this.transform).transform));
                powerUpType = GameManager.PowerUpEnum.Score;
                break;
            default:
                break;
        }

    }

    private IEnumerator Inflate(Transform powerUp)
    {
        Vector3 initSacle = powerUp.localScale;
        powerUp.localScale = Vector3.zero;
        while (powerUp.localScale.magnitude <= initSacle.magnitude)
        {
            powerUp.localScale = initSacle / inflationTime;
            inflationTime -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        inflationTime = 2;
    }
}
