﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{

    [SerializeField] float characterMovementSpeed = 2;
    [SerializeField] float characterRotationSpeed = 10;

    private float move;
    private float strafe;
    private float rotate;
    private Rigidbody body;
    private Animator animator;
    private AudioSource audioSource;

    public delegate void PickUpAction(PowerUp powerUpItem, Transform powerUpTransform);
    public static event PickUpAction onPickUp;


    private void OnTriggerEnter(Collider other)
    {
        var spawner = other.transform.parent.GetComponent<PowerUpSpawner>();
        spawner.OnPickUp();
        onPickUp(spawner.powerUpSOList[(int)spawner.powerUpType], other.transform);
     
        audioSource.PlayOneShot(spawner.powerUpSOList[(int)spawner.powerUpType].powerUpSound, 0.7f);
        Destroy(other.gameObject);

    }
    // Use this for initialization
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        Camera.main.gameObject.GetComponent<LookAtCharacter>().AssignTarget(this.transform);
        animator = transform.GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        move = Input.GetAxis("Vertical");
        strafe = Input.GetAxis("Horizontal");
        rotate = Input.GetAxis("Rotational");
        if (move == 0 && strafe == 0 && rotate == 0)
        {
            animator.SetBool("isMoving", false);

        }
        else
        {
            animator.SetBool("isMoving", true);

        }


    }

    // Update is called once per frame
    void Update()
    {
        body.transform.Translate(body.transform.right * characterMovementSpeed * strafe * Time.deltaTime, Space.World);
        transform.Rotate(transform.up, rotate * characterRotationSpeed * Time.deltaTime);
        body.transform.Translate(body.transform.forward * characterMovementSpeed * move * Time.deltaTime, Space.World);

    }


}
