﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public enum PowerUpEnum
    {
        Time,
        Score
    }

    [SerializeField] private float time = 100;
    private float score;
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    private void OnEnable()
    {
        PlayerControl.onPickUp += UsePowerUp;

    }

    private void UsePowerUp(PowerUp powerUpItem, Transform powerUp)
    {
        time += powerUpItem.time;
        score += powerUpItem.score;
    }

    //Restrict Instantiating
    private GameManager()
    {
    }
    private void Start()
    {
        StartCoroutine(Timer());
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnGUI()
    {
        timerText.text = Mathf.RoundToInt(time).ToString();
        scoreText.text = score.ToString();
    }

    private IEnumerator Timer()
    {
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }


    }
    public void Retry()
    {
        StartCoroutine(UnloadScene());
        StartCoroutine(LoadScene());
    }

    public void Home()
    {
        StartCoroutine(UnloadScene());
        StartCoroutine(LoadMenu());
    }
    private void AddExtraTime(float extraTime)
    {
        time += extraTime;
    }
}
