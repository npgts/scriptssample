﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "PowerUp")]
public class PowerUp : ScriptableObject
{

    public float time;
    public float score;
    public GameObject powerUpGameOBject;
    public AudioClip powerUpSound;
    public ParticleSystem pickUpEffect;
}
