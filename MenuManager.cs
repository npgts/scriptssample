﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;


public class MenuManager : MonoBehaviour
{

    public static MenuManager instance;
    public MenuManager Instance { get { return instance; } }

    //simple singleton
    private MenuManager()
    {

    }
    public void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void OnStart()
    {
        StartCoroutine(LoadScene());
        StartCoroutine(UnloadMenu());
    }
    private IEnumerator UnloadMenu()
    {
        SceneManager.UnloadSceneAsync("mainMenu");
        yield return null;
    }
    private IEnumerator LoadScene()
    {
        SceneManager.LoadSceneAsync("gameScene");
        yield return null;

    }
    public void OnExit()
    {
        Application.Quit();
    }

}
